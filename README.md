![](doc/images/ggpo_header.png)

&nbsp; _[![Appveyor build status](https://img.shields.io/appveyor/ci/pond3r/ggpo/master.svg?logo=appveyor)](https://ci.appveyor.com/project/pond3r/ggpo/branch/master)_

### Note: This is a fork of the branch libsdlvw-rfc of the fork made by Github user Shugyousha. This fork offers Linux cross-platform support.
### Link to parent fork: https://github.com/Shugyousha/ggpo/tree/libsdlvw-rfc

## What's GGPO?
Traditional techniques account for network transmission time by adding delay to a players input, resulting in a sluggish, laggy game-feel.  Rollback networking uses input prediction and speculative execution to send player inputs to the game immediately, providing the illusion of a zero-latency network.  Using rollback, the same timings, reactions visual and audio queues, and muscle memory your players build up playing offline translate directly online.  The GGPO networking SDK is designed to make incorporating rollback networking into new and existing games as easy as possible.  

For more information about the history of GGPO, check out http://ggpo.net/

This repository contains the code, documentation, and sample applications for the SDK.

## Building
Building GGPO is currently only available on Windows, however efforts are being made to port it to other platforms.

### Windows Visual Studio 2019
Windows builds requires both [Visual Studio 2019](https://visualstudio.microsoft.com/downloads/) and [CMake](https://cmake.org/download/).  Make sure you've installed both before starting.  Make sure to add CMake to your path when installing.

- Run the `build_windows.cmd` in the root directory of the SDK to generate the Visual Studio 2019 solution files.   
- Open `build/GGPO.sln` solution for Visual Studio 2019 to compile.

You can also run the `cmake-gui` tool if you prefer. 

### Windows MinGW 
Windows build instructions for MinGW toolchain. 

If using MSYS
```
$ cd build
$ cmake -G "MSYS Makefiles" .. 
$ make
```

### Linux

```
$ cd build
$ cmake ..
$ make
```

The VectorWar executable is in src/apps/vectorwar. Use commands from the scripts in start_vectorwar_2p.sh to get started with testing.

Example: In build/src/apps/vectorwar. Run these 2 commands.
```
$ ./VectorWar 7000 2 local 127.0.0.1:7001 &
$ ./VectorWar 7001 2 127.0.0.1:7000 local &
```

## Sample Application
The Vector War application in the source directory contains a simple application which uses GGPO to synchronize the two clients.  The command line arguments are:

```
vectorwar.exe  <localport>  <num players> ('local' | <remote ip>:<remote port>) for each player
```

See the .cmd files in the bin directory for examples on how to start 2, 3, and 4 player games.

## Licensing
GGPO is available under The MIT License. This means GGPO is free for commercial and non-commercial use. Attribution is not required, but appreciated. 